package edu.upenn.cis.cis550.client;

import java.util.List;

import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.cis550.shared.Actor;

/**
 * Renders a table of actors
 * 
 * @author zives
 *
 */
public class ActorsPage {
	List<Actor> actors;
	String query;
	
	final Button againButton = new Button("Return");

	CellTable<Actor> actorTable = new CellTable<Actor>();
	ListDataProvider<Actor> actorProvider;
	
	/**
	 * These are definitions for the CellTable of what to put in
	 * each column
	 */
	
	
	TextColumn<Actor> firstNameColumn = new TextColumn<Actor>() {

		@Override
		public String getValue(Actor object) {
			return object.getFirstName();
		}
		
	};
	TextColumn<Actor> lastNameColumn = new TextColumn<Actor>() {

		@Override
		public String getValue(Actor object) {
			return object.getLastName();
		}
		
	};
	TextColumn<Actor> genderColumn = new TextColumn<Actor>() {

		@Override
		public String getValue(Actor object) {
			return object.getGender();
		}
		
	};

	/**
	 * Constructor for actors page
	 * 
	 * @param query the term we searched for
	 * @param actors list of actors returned
	 */
	public ActorsPage(String query, List<Actor> actors) {
		this.actors = actors;
		this.query = query;
		
		// We can add style names to widgets
		againButton.addStyleName("sendButton");

		// clear the contents of the main page
		RootPanel.get("messageContainer").clear();
		RootPanel.get("nameFieldContainer").clear();
		RootPanel.get("sendButtonContainer").clear();
		RootPanel.get("errorLabelContainer").clear();

		// Add our label, table, and button
		RootPanel.get("messageContainer").add(new Label("Actors with last name " + query + ":"));
		RootPanel.get("nameFieldContainer").add(actorTable);
		RootPanel.get("errorLabelContainer").add(againButton);
		
		// Format the table to show 3 columns from Actors
		actorTable.addColumn(firstNameColumn, "First name");
		actorTable.addColumn(lastNameColumn, "Last name");
		actorTable.addColumn(genderColumn, "Gender");
		
		// Create a data provider that connects from the List of Actors
		// to the actual CellTable
		actorProvider = new ListDataProvider<Actor>();
		actorProvider.addDataDisplay(actorTable);
	}
	
	/**
	 * Does the majority of the active work for the Actors page,
	 * by populating the table and handling button clicks
	 */
	public void doWork() {
		// The ListDataProvider has an internal list; populate it
		List<Actor> output = actorProvider.getList();
		output.addAll(actors);

		// If the user clicks "Return" we switch back to the index page
		againButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				IndexPage ip = new IndexPage();
				ip.doWork();
			}
			
		});
	}
	
}
