package edu.upenn.cis.cis550.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.cis550.shared.Actor;
import edu.upenn.cis.cis550.shared.exceptions.DatabaseException;

/**
 * The client side stub for the RPC service.
 * 
 */
@RemoteServiceRelativePath("imdb")
public interface MyService extends RemoteService {
	List<Actor> getActorsWithLastName(String name) 
			throws IllegalArgumentException, DatabaseException;
}
