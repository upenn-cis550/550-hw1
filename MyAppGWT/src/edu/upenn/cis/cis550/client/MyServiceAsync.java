package edu.upenn.cis.cis550.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.cis550.shared.Actor;

/**
 * The async counterpart of <code>MyService</code>.
 */
public interface MyServiceAsync {

	void getActorsWithLastName(String name, AsyncCallback<List<Actor>> callback);
}
