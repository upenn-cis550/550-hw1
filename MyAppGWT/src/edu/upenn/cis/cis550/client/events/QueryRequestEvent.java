package edu.upenn.cis.cis550.client.events;

import java.util.List;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.cis550.shared.Actor;

/**
 * This represents an event to be passed along the EventBus.
 * The particular event is that "someone" is requesting a query
 * for actors by lastname
 *  
 * @author zives
 *
 */
public class QueryRequestEvent extends GwtEvent<QueryRequestEvent.Handler> {
	// Query content
	private String lastName;
	
	// Whom to call when results are returned
	private AsyncCallback<List<Actor>> callback; 
	
	/**
	 * A request event for a new query
	 * 
	 * @param lastname Actor last name to look up
	 * @param callback Whom to call with returned results
	 */
	public QueryRequestEvent(String lastname, AsyncCallback<List<Actor>> callback) {
		lastName = lastname;
		this.callback = callback;
	}
	
	/**
	 * GwtEvents need to make clear what their type and handler type is
	 */
	public static Type<QueryRequestEvent.Handler> TYPE = 
			new Type<QueryRequestEvent.Handler>();
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	/**
	 * When an event is fired, we call this method
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.processRequestForActor(lastName, callback);
	}
	
	/**
	 * An event handler for the query request event
	 * 
	 * @author zives
	 *
	 */
	public static interface Handler extends EventHandler {
		public void processRequestForActor(String lastName, AsyncCallback<List<Actor>> callback);
	}


}
