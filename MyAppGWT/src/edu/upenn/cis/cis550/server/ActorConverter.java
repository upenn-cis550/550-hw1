package edu.upenn.cis.cis550.server;

import java.util.List;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.upenn.cis.cis550.shared.Actor;

/**
 * This is a utility class to manage the conversion of database tuples
 * into Actor object (lists)
 * 
 * @author zives
 *
 */
public class ActorConverter {
	/**
	 * Converts the next tuple in a ResultSet into an Actor object
	 * @param rs input ResultSet
	 * @return corresponding Actor object
	 * @throws SQLException
	 */
	public static Actor getActorFrom(ResultSet rs) throws SQLException {
		return new Actor(rs.getInt("ID"), rs.getString("FIRST_NAME"),
				rs.getString("LAST_NAME"), rs.getString("GENDER"));
	}
	
	/**
	 * reads a ResultSet stream and returns a list of Actor objects
	 * @param rs input ResultSet
	 * @return list of corresponding Actors
	 * @throws SQLException 
	 */
	public static List<Actor> getActorListFrom(ResultSet rs) throws SQLException {
		List<Actor> ret = new ArrayList<Actor>();
		while (rs.next()) {
			ret.add(getActorFrom(rs));
		}
		return ret;
	}
}
