package edu.upenn.cis.cis550.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import edu.upenn.cis.cis550.client.MyService;
import edu.upenn.cis.cis550.shared.Actor;
import edu.upenn.cis.cis550.shared.exceptions.DatabaseException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class MyServiceImpl extends RemoteServiceServlet implements
		MyService {
	
	Connection conn;
	Statement stat;
	static final String hostName = "upenn-cis-550.cpthhl13obrz.us-east-1.rds.amazonaws.com";
	static final String user = "student";
	static final String password = "cis550hw1";
	static final String database = "ORCL"; 
	
	
	public MyServiceImpl() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.OracleDriver");
		conn = DriverManager.getConnection("jdbc:oracle:thin:@//" + hostName
				+ "/" + database, user, password);
		
		stat = conn.createStatement();
	}

	public String greetServer(String input) throws IllegalArgumentException {
		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo
				+ ".<br><br>It looks like you are using:<br>" + userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	@Override
	public List<Actor> getActorsWithLastName(String name)
			throws IllegalArgumentException, DatabaseException {
		ResultSet rset;
		try {
			rset = stat.executeQuery(
					"SELECT * FROM awsuser.actors WHERE last_name='" + name + 
					"' AND rownum <= 10");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DatabaseException(e.getMessage());
		}
		if (rset != null)
			try {
				return ActorConverter.getActorListFrom(rset);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new DatabaseException("Unable to convert to Actor -- schema error?");
			}
		else
			return null;
	}
}
