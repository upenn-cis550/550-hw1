package edu.upenn.cis.cis550.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This basic class is a Data Transfer Object for
 * representing an IMDB Actor
 * 
 * @author zives
 *
 */
public class Actor implements IsSerializable {
	int id;
	String firstName;
	String lastName;
	String gender;
	
	/**
	 * Standard empty-constructor
	 */
	public Actor() {
		super();
		id = 0;
		firstName = "";
		lastName = "";
		gender = "";
	}
	
	/**
	 * Constructor for an Actor with known values
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param gender
	 */
	public Actor(int id, String firstName, String lastName, String gender) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}
