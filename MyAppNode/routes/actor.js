// Connect string to Oracle
var connectData = { 
  "user": "student", 
  "password": "cis550hw1", 
  "connectString": "RDS"
};
var oracle =  require("oracledb");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(res,name) {
  oracle.getConnection(connectData, function(err, connection) {
    if ( err ) {
    	console.log("Error: " + err);
    } else {
	  	// selecting rows
	  	connection.execute("SELECT * FROM awsuser.actors WHERE last_name='" + name + 
	  			"' AND rownum <= 10", 
	  			   [], 
	  			   { //resultSet: true,
	  			     outFormat: oracle.OBJECT }, // return a result set
	  			   function(err, result) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    } else {
    			output_actors(res, name, result.rows);
	  	    	connection.release(
	  	    		function(err) {
		  	    		if (err)
		  	    			console.error(err.message);
		  	    		else
		  	    			console.log("Connection closed");
		  	    	}); // done with the connection
	  	    		
	  	    }
	
	  	}); // end connection.execute
    }
  }); // end oracle.connect
}

/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function output_actors(res,name,results) {
	res.render('actor.jade',
		   { title: "Actors with last name " + name,
		     results: results }
	  );
}

/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(res,req.query.name);
};
